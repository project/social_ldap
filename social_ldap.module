<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * For user_login_form.
 */
function social_ldap_form_social_user_login_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // @see https://www.drupal.org/project/social/issues/3023952:
  _social_ldap_authentication_login_form_alter($form, $form_state, $form_id);
}

/**
 * Helper function for the user login block.
 *
 * Relevant in ldap_authn_form_user_login_block_alter and
 * ldap_authn_form_user_login_alter.
 *
 * @param array $form
 *   The form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 * @param string $form_id
 *   The form ID.
 */
function _social_ldap_authentication_login_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  // Our validate callback has to run first to map the missing values:
  array_unshift($form['#validate'], 'social_ldap_authentication_user_login_authenticate_validate');

  // We also have to add ldap_user_grab_password_validate to the custom form:
  array_unshift($form['#validate'], 'ldap_user_grab_password_validate');

  // Delegate to the helper function from ldap_authentication for the
  // custom Social login form:
  _ldap_authentication_login_form_alter($form, $form_state, $form_id);

  // Set the description of the name and pass fields:
  if (isset($form['username_login'])) {
    if (isset($form['name']['#description'])) {
      $form['username_login']['name_or_mail']['#description'] = $form['name']['#description'];
    }
    if (isset($form['pass']['#description'])) {
      $form['username_login']['pass']['#description'] = $form['pass']['#description'];
    }
  }
  elseif (isset($form['name_or_mail'])) {
    // Set description for older open social versions.
    $form['name_or_mail']['#description'] = $form['name']['#description'];
  }
}

/**
 * Validate function for user logon forms.
 *
 * @param array $form
 *   The form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function social_ldap_authentication_user_login_authenticate_validate(array $form, FormStateInterface $form_state) {
  // Social defínes it own social_user_login_form instead of user_login_form.
  // ldap_authentication isn't aware of that form
  // Furthermore ldap_authentication has hard coded form 'name' field
  // dependencies in code in module and services.
  // So currently we have to map 'name_or_email' to 'name':
  if ($form_state->hasValue('name_or_mail')) {
    $name_or_mail = $form_state->getValue('name_or_mail');

    // Try to retrieve the account name by mail address:
    $userStorage = \Drupal::service('entity_type.manager')->getStorage('user');
    $accounts = $userStorage->loadByProperties(['mail' => $name_or_mail, 'status' => 1]);
    $account = reset($accounts);
    if (!empty($account)) {
      $name_or_mail = $account->getAccountName();
    }

    $form_state->setValue('name', $name_or_mail);
  }
}
